package sel.zalenium;

import java.util.PriorityQueue;
import java.util.Random;

public class KLargestNumberFromArray {

	public static void main(String[] args) {

		PriorityQueue<Integer> p = new PriorityQueue<Integer>(10);

		Random rd = new Random(); // creating Random object
		int[] arr = new int[50000];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = rd.nextInt();

		}

		for (int i = 0; i < 10; i++) {
			p.add(arr[i]);
		}

		for (int i = 10; i < arr.length; i++) {
			if (p.peek() < arr[i]) {
				p.poll();
				p.add(arr[i]);
			}
		}
		

		while (p.size()!=0) {
			System.out.println(p.poll());
		}
	}

}
